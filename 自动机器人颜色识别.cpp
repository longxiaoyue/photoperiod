#include "stdafx.h"
#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include<windows.h>

using namespace cv;
using namespace std;

int flag = 0;
int roi_x = 50;//设置感兴趣区域位置
int roi_y = 60;
int roi_w = 4;//设置感兴趣区域大小
int roi_h = 4;
float wucha = 0.1f;//0.2即允许0.2		
int hong_yuzhi[6] = { 19,	31	,148,	53,	56	,148
};//红色色彩测量值,b,g,r.
int lan_yuzhi[3] = { 0.57,0.3,0.13 };//蓝色色彩测量值,b,g,r.
float jin_yuzhi[3] = { 0.16,0.41,0.42 };//金色色彩测量值,b,g,r.

int hlow = 38, slow = 0, vlow = 150;


int getrgb(Mat img, int num)//图片和通道选择
{
	long temp = 0;
	for (int i = 0; i < img.rows; i++)
		for (int j = 0; j < img.cols; j++)
			temp += img.at<Vec3b>(i, j)[num];
	return temp / img.rows / img.cols;
}
int Chi(Mat input, Mat output)
{
	//Mat to IplImzge 深拷贝
	IplImage imgTmp = input;
	IplImage* src = &imgTmp;

	//Mat to IplImzge 浅拷贝
	IplImage imgTmp1 = output;
	IplImage *dst = &imgTmp;

	int height = src->height;
	int width = src->width;
	int step = src->widthStep;
	int i = 0, j = 0;
	unsigned char R, G, B, MaxC;
	double alpha, beta, alpha_r, alpha_g, alpha_b, beta_r, beta_g, beta_b, temp = 0, realbeta = 0, minalpha = 0;
	double gama, gama_r, gama_g, gama_b;
	unsigned char* srcData;
	unsigned char* dstData;

	for (i = 0; i < height; i++)
	{
		srcData = (unsigned char*)src->imageData + i * step;
		dstData = (unsigned char*)dst->imageData + i * step;
		for (j = 0; j < width; j++)
		{
			R = srcData[j * 3];
			G = srcData[j * 3 + 1];
			B = srcData[j * 3 + 2];
			alpha_r = (double)R / (double)(R + G + B);
			alpha_g = (double)G / (double)(R + G + B);
			alpha_b = (double)B / (double)(R + G + B);

			alpha = max(max(alpha_r, alpha_g), alpha_b);

			double X = 150 / alpha;


			if ((R + G + B) > hlow)
			{
				dstData[j * 3] = alpha_r * X;
				dstData[j * 3 + 1] = alpha_g * X;
				dstData[j * 3 + 2] = alpha_b * X;
			}
			else
			{
				dstData[j * 3] = srcData[j * 3];
				dstData[j * 3 + 1] = srcData[j * 3 + 1];
				dstData[j * 3 + 2] = srcData[j * 3 + 2];
			}


		}

	}

	//cvShowImage("src", src);

	//cvShowImage("dst", dst);



	return 1;

}
void simp_sun(Mat input) {
	//Mat to IplImzge 深拷贝
	IplImage imgTmp = input;
	IplImage* src = &imgTmp;


	int height = src->height;
	int width = src->width;
	int step = src->widthStep;
	int i = 0, j = 0;
	int R, G, B, A;
	unsigned char* srcData;

	for (i = 0; i < height; i++)
	{
		srcData = (unsigned char*)src->imageData + i * step;
		for (j = 0; j < width; j++)
		{
			R = srcData[j * 3];
			G = srcData[j * 3 + 1];
			B = srcData[j * 3 + 2];
			if ((R + G + B) > hlow)
			{
				A = max(max(R, G), B);//求最大值

				srcData[j * 3 + 0] = 150 * R / (A + 1);
				srcData[j * 3 + 1] = 150 * G / (A + 1);
				srcData[j * 3 + 2] = 150 * B / (A + 1);
			}
		}
	}

}
void ConnectedComponentsStats(Mat img)
{

	Mat lables, stats, centroids;
	int num_objects = connectedComponentsWithStats(img, lables, stats, centroids);
	//看看有几个目标,统计连通区域信息
	if (num_objects < 2)
		cout << "NO objects deceted" << endl;//只有背景
	else
	{
		cout << "number if objects dected:" << num_objects - 1 << endl;

	}
	//创建彩色对象的输出图像，并显示连通区域
	Mat output4 = Mat::zeros(img.rows, img.cols, CV_8UC3);
	RNG rng(0xffffffff);
	for (int i = 1; i < num_objects; i++)//遍历除0以外的所有标签，0是背景
	{
		//		cout << "object" << i << "with pos:" << centroids.at<Point2d>(i)
		//		<< "with area" << stats.at<int>(i, CC_STAT_AREA) << endl;
		Mat mask = lables == i;
		output4.setTo(Scalar(255), mask);
		//使用区域绘制的文本
		stringstream ss;
		ss << "area:" << stats.at<int>(i, CC_STAT_AREA);
		putText(
			output4,
			ss.str(),
			centroids.at<Point2d>(i),
			FONT_HERSHEY_SIMPLEX,
			0.4,
			Scalar(100, 100, 100)
		);

		imshow("面积,", output4);
	}
}
Mat Contours(Mat inrange, Rect &maxRect)
{
	Mat  open, close;
	int area[1010], i;
	int areamax;
	Mat Ctimg;//连通域图
	stringstream ss;

	/*形态学操作*/
	Mat element = getStructuringElement(MORPH_RECT, Size(4, 4));
	morphologyEx(inrange, close, MORPH_CLOSE, element);
	morphologyEx(close, open, MORPH_OPEN, element);

	/*轮廓寻找与面积的计算*/
	vector<Point> points;
	vector<vector<cv::Point>> contours;//cv::很重要，不然可能出问题:原因是标准库里有std::vector 和 Point 和findContours里要用到的vector和Point不是一回事所以，声明的时候要用cv::vector和cv::Point就可以了
	vector<Vec4i> hierarchy;
	findContours(open, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	double maxArea = 0;
	for (long i = 0; i < contours.size(); i++)
	{
		double each = contourArea(contours[i]);
		if (each > maxArea)
		{
			maxArea = each;
			points = contours[i];
		}
	}
	maxRect = boundingRect(points);
	if (maxRect.area() > 50)//加上判断，防止在丢失目标时还画框
	{
		rectangle(open, maxRect, Scalar(255));
		ss << "__" << maxArea;
		putText(open,        //目标图片
			ss.str(),     //待绘制的文字
			Point(maxRect.x, maxRect.y),//左下角
			FONT_HERSHEY_COMPLEX_SMALL, //字体
			1,//尺寸因子，值越大字体越大,double
			Scalar(255, 255, 255),
			1,//线条宽度
			4, //线型，4或8邻域
			0);//1,反转180°,0不反转
	}
	return open;
}
void Control(Mat Ctimg, Rect maxrect, uint8_t*dat)
{
	int vgo = 0;//前进速度+-600
	int vpitch = 0;//俯速度+-600
	int vyaw = 0;//左转速度+-600
	/*偏差计算*/
	int width = Ctimg.cols;//屏幕宽度
	int height = Ctimg.rows;//屏幕高度
	Point2i center = Point2i(maxrect.x + maxrect.width / 2, maxrect.y + maxrect.height / 2);//目标中心
	int Dx = -(width / 2 - center.x);//水平方向差值
	int Dy = -(height / 2 - center.y);//垂直方向差值
	/*速度计算*/
	vyaw = 1024 - (Dx * 1200 / width);
	vpitch = 1024 + Dy * 1200 / height;
	if (abs(vyaw - 1024) < 100)
		vgo = 1024 + 400;//匀速
	else
		vgo = 1024 + 0;

	/*速度转换到结构体*/
	dat[3] = (uint8_t)(vgo & 0x00ff);
	dat[2] = (uint8_t)((vgo >> 8) & 0x00ff);
	dat[5] = (uint8_t)(vyaw & 0x00ff);
	dat[4] = (uint8_t)((vyaw >> 8) & 0x00ff);
	dat[7] = (uint8_t)(vpitch & 0x00ff);
	dat[6] = (uint8_t)((vpitch >> 8) & 0x00ff);
}
uint16_t CRC_INIT = 0xffff;
const uint16_t wCRC_Table[256] =
{
	0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
	0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
	0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
	0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
	0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
	0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
	0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
	0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
	0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
	0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
	0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
	0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
	0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
	0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
	0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
	0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
	0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
	0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
	0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
	0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
	0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
	0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
	0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
	0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
	0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
	0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
	0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
	0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
	0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
	0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
	0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
	0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};
/*
** Descriptions: CRC16 checksum function   描述：CRC16校验和功能
** Input: Data to check,Stream length, initialized checksum  输入：要检查的数据，流长度，已初始化的校验和
** Output: CRC checksum   输出：CRC校验和
*/
uint16_t Get_CRC16_Check_Sum(uint8_t *pchMessage, uint32_t dwLength, uint16_t wCRC)
{
	uint8_t chData;
	if (pchMessage == NULL)
	{
		return 0xFFFF;
	}
	while (dwLength--)
	{
		chData = *pchMessage++;
		(wCRC) = ((uint16_t)(wCRC) >> 8) ^ wCRC_Table[((uint16_t)(wCRC) ^ (uint16_t)(chData)) &
			0x00ff];
	}
	return wCRC;
}
/*
** Descriptions: CRC16 Verify function   描述：CRC16验证功能
** Input: Data to Verify,Stream length = Data + checksum  输入：要验证的数据，流数据=数据+校验和
** Output: True or False (CRC Verify Result)  输出：真或假（CRC校验结果）
*/
uint32_t Verify_CRC16_Check_Sum(uint8_t *pchMessage, uint32_t dwLength)
{
	uint16_t wExpected = 0;
	if ((pchMessage == NULL) || (dwLength <= 2))
	{
		return 0;
	}
	wExpected = Get_CRC16_Check_Sum(pchMessage, dwLength - 2, CRC_INIT);
	return ((wExpected & 0xff) == pchMessage[dwLength - 2] && ((wExpected >> 8) & 0xff) ==
		pchMessage[dwLength - 1]);
}
/*
** Descriptions: append CRC16 to the end of data   描述：将CRC16附加到数据结尾
** Input: Data to CRC and append,Stream length = Data + checksum   输入：数据到CRC并追加，流长度=数据+校验和
** Output: True or False (CRC Verify Result)   输出真或假（CRC）检验结果
*/
void Append_CRC16_Check_Sum(uint8_t * pchMessage, uint32_t dwLength)
{
	uint16_t wCRC = 0;
	if ((pchMessage == NULL) || (dwLength <= 2))
	{
		return;
	}
	wCRC = Get_CRC16_Check_Sum((uint8_t *)pchMessage, dwLength - 2, CRC_INIT);
	pchMessage[dwLength - 2] = (uint8_t)(wCRC & 0x00ff);
	pchMessage[dwLength - 1] = (uint8_t)((wCRC >> 8) & 0x00ff);
}
//--------------------------------【on_MouseHandle( )函数】-----------------------------
//		描述：鼠标回调函数，根据不同的鼠标事件进行不同的操作
//-----------------------------------------------------------------------------------------------
Rect roi;
bool g_bDrawingBox = false;

void on_MouseHandle(int event, int x, int y, int flags, void* param)
{

	Mat& image = *(cv::Mat*) param;
	switch (event)
	{
		//鼠标移动消息
	case EVENT_MOUSEMOVE:
	{

		if (g_bDrawingBox)//如果是否进行绘制的标识符为真，则记录下长和宽到RECT型变量中
		{
			roi.width = x - roi.x;
			roi.height = y - roi.y;

		}
	}
	break;

	//左键按下消息
	case EVENT_LBUTTONDOWN:
	{
		g_bDrawingBox = true;
		roi = Rect(x, y, 0, 0);//记录起始点

	}
	break;

	//左键抬起消息
	case EVENT_LBUTTONUP:
	{
		g_bDrawingBox = false;//置标识符为false
							  //对宽和高小于0的处理
		if (roi.width < 0)
		{
			roi.x += roi.width;
			roi.width *= -1;
		}

		if (roi.height < 0)
		{
			roi.y += roi.height;
			roi.height *= -1;
		}
		//调用函数进行绘制
		//	DrawRectangle(image, roi);
		//cout << roi << endl;

	}
	break;

	}
}
//-----------------------------------【DrawRectangle( )函数】------------------------------
//		描述：自定义的矩形绘制函数
//-----------------------------------------------------------------------------------------------
void DrawRectangle(cv::Mat& img, cv::Rect box)
{
	rectangle(img, box.tl(), box.br(), Scalar(100));//随机颜色
}


int main()
{

	/*摄像机参数配置*/
	VideoCapture cap(0);
	cap.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 160);//设置宽像素
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 120);//设置高像素
	cap.set(CV_CAP_PROP_WHITE_BALANCE_BLUE_U, 6500);//设置白平衡值，防止自动白平衡干扰
	//cap.set(CV_CAP_PROP_AUTO_EXPOSURE,-6);
	if (cap.isOpened())
		cout << "cam open success/n";
	/* ~~~~~~~~~串口线程配置~~~~~~~~~*/
	CSerialPort uart00;
	if (!uart00.InitPort(4, 115200))
		cout << "串口打开失败" << endl;
	if (!uart00.OpenListenThread())
		cout << "开启监听失败" << endl;

	unsigned char data0[3] = { 0xAA,0x00,0x55 };
	unsigned char data1[3] = { 0xAA,0x01,0x55 };
	unsigned char data2[3] = { 0xAA,0x02,0x55 };
	unsigned char data3[3] = { 0xAA,0x03,0x55 };
	uint8_t pack1[10] = { 0xa5,0x5a,0x04,0x00,0x04,0x00,0x04,0x00 };

	char readdata[3];
	char exreaddata[3] = { 0xAA,0x20,0x55 };
	/* ~~~~~~~~~变量定义~~~~~~~~~*/
	int yaw = 1024;
	int pitch = 1024;

	//namedWindow("阈值");
	//createTrackbar("hlow", "阈值", &hlow, 255);
	//createTrackbar("yaw", "阈值", &yaw, 2048);
	//createTrackbar("pitch", "阈值", &pitch, 2048);
	Mat srcimg,//原图
		strimg, //增强图
		binimg, //二值图
		OCimg,//开闭操作图
		Ctimg;//连通域图与目标方框
	Rect maxRect;
	vector<Mat> hsvsplit;


	Mat ERODE;
	Mat rect;

	namedWindow("in");
	setMouseCallback("in", on_MouseHandle, (void*)&simp_sun);

	while (1)
	{
		cap >> srcimg;

		strimg = srcimg.clone();
		simp_sun(strimg);//转换去光照
		//Chi(strimg, strimg);
		static Rect box_rect = Rect(strimg.cols / 2, strimg.rows / 2, roi_w, roi_h);
		if(roi.width>0&&roi.height>0)
		box_rect = roi;//鼠标函数
		rect = strimg(box_rect);
		if (waitKey(1) == 's')//采集RGB
			cout << getrgb(rect, 0) << '\t' << getrgb(rect, 1) << '\t' << getrgb(rect, 2) << endl;

		inRange(strimg, Scalar(hong_yuzhi[0], hong_yuzhi[1], hong_yuzhi[2]), Scalar(hong_yuzhi[3], hong_yuzhi[4], hong_yuzhi[5]), binimg);
		Ctimg = Contours(binimg, maxRect);//求连通域
		Control(Ctimg, maxRect, pack1);//将目标数据转化成可使用数据
		Append_CRC16_Check_Sum((uint8_t*)&pack1, sizeof(pack1));//结构体内元素按所占空间最大的对齐，所以变量类型应尽量一致，或者使用数组		cout << sizeof(data8);
		if (waitKey(50) == 'z')
		{
			cout << pack1[2] * 255 + pack1[3] << endl;
			cout << pack1[4] * 255 + pack1[5] << endl;
			cout << pack1[6] * 255 + pack1[7] << endl;
		}
		uart00.WriteData(pack1, 10);
		//imshow("bin", binimg);
		rectangle(srcimg, box_rect, Scalar(255), 2);
		imshow("out", strimg);
		imshow("in", srcimg);
		imshow("Ctimg", Ctimg);
		cvMoveWindow("in", 0, 0);
		cvMoveWindow("out", 320, 0);
		cvMoveWindow("Ctimg", 0, 240);
	}//主循环截至位

}